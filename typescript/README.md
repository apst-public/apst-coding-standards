# Asia Pacific Tech/ SMBApps Typescript Style Guide

Asia Pacific Tech/ SMBApps uses [Airbnb style guide for Javascript](../javascript/) along with the following rules from Google style guide for Typescript.


 ## Type parameters

Type parameters, like in `Array<T>`, may use a single upper case character (`T`) or `UpperCamelCase`.


 ## Naming Convention for interface

 Do not mark interfaces specially (`IMyInterface` or `MyFooInterface`) unless it's idiomatic in its environment. When introducing an interface for a class, give it a name that expresses why the interface exists in the first place (e.g. `class TodoItem` and `interface TodoItemStorage` if the interface expresses the format used for storage/serialization in JSON).

## Omit comments that are redundant with TypeScript
 For example, do not declare types in `@param` or `@return` blocks, do not write `@implements`, `@enum`, `@private` etc. on code that uses the `implements`, `enum`, `private` etc. keywords.

## Do not use `@override`

 Do not use `@override` in TypeScript source code.

`@override` is not enforced by the compiler, which is surprising and leads to annotations and implementation going out of sync. Including it purely for documentation purposes is confusing.

Language Rules
--------------

## Visibility

Restricting visibility of properties, methods, and entire types helps with keeping code decoupled.

*   Limit symbol visibility as much as possible.
*   Consider converting private methods to non-exported functions within the same file but outside of any class, and moving private properties into a separate, non-exported class.
*   TypeScript symbols are public by default. Never use the `public` modifier except when declaring non-readonly public parameter properties (in constructors).
```
    class Foo {
      public bar = new Bar();  // BAD: public modifier not needed
    
      constructor(public readonly baz: Baz) {}  // BAD: readonly implies it's a property which defaults to public
    }
    

    class Foo {
      bar = new Bar();  // GOOD: public modifier not needed
    
      constructor(public baz: Baz) {}  // public modifier allowed
    }
```    

See also [export visibility](#export-visibility) below.

## Constructors

Constructor calls must use parentheses, even when no arguments are passed:
```
    const x = new Foo;
    

    const x = new Foo();
 ```   

It is unnecessary to provide an empty constructor or one that simply delegates into its parent class because ES2015 provides a default class constructor if one is not specified. However constructors with parameter properties, modifiers or parameter decorators should not be omitted even if the body of the constructor is empty.
```
    class UnnecessaryConstructor {
      constructor() {}
    }
    

    class UnnecessaryConstructorOverride extends Base {
        constructor(value: number) {
          super(value);
        }
    }
    

    class DefaultConstructor {
    }
    
    class ParameterProperties {
      constructor(private myService) {}
    }
    
    class ParameterDecorators {
      constructor(@SideEffectDecorator myService) {}
    }
    
    class NoInstantiation {
      private constructor() {}
    }
 ```   

## Class Members

### No `#private` fields

Do not use private fields (also known as private identifiers):
```
    class Clazz {
      #ident = 1;
    }
 ```   

Instead, use TypeScript's visibility annotations:
```
    class Clazz {
      private ident = 1;
    }
  ```  

Why?

Private identifiers cause substantial emit size and performance regressions when down-leveled by TypeScript, and are unsupported before ES2015. They can only be downleveled to ES2015, not lower. At the same time, they do not offer substantial benefits when static type checking is used to enforce visibility.

### Use `readonly`

Mark properties that are never reassigned outside of the constructor with the `readonly` modifier (these need not be deeply immutable).

### Parameter properties

Rather than plumbing an obvious initializer through to a class member, use a TypeScript [parameter property](https://www.typescriptlang.org/docs/handbook/classes.html#parameter-properties).

```
    class Foo {
      private readonly barService: BarService;
    
      constructor(barService: BarService) {
        this.barService = barService;
      }
    }
    

    class Foo {
      constructor(private readonly barService: BarService) {}
    }
  ```  

If the parameter property needs documentation, [use an `@param` JSDoc tag](#parameter-property-comments).

#### Field initializers

If a class member is not a parameter, initialize it where it's declared, which sometimes lets you drop the constructor entirely.
```
    class Foo {
      private readonly userList: string[];
      constructor() {
        this.userList = [];
      }
    }
    

    class Foo {
      private readonly userList: string[] = [];
    }
```

### Properties used outside of class lexical scope

Properties used from outside the lexical scope of their containing class, such as an AngularJS controller's properties used from a template, must not use `private` visibility, as they are used outside of the lexical scope of their containing class.

Prefer `public` visibility for these properties, however `protected` visibility can also be used as needed. For example, Angular and Polymer template properties should use `public`, but AngularJS should use `protected`.

TypeScript code must not not use `obj['foo']` to bypass the visibility of a property

Why?

When a property is `private`, you are declaring to both automated systems and humans that the property accesses are scoped to the methods of the declaring class, and they will rely on that. For example, a check for unused code will flag a private property that appears to be unused, even if some other file manages to bypass the visibility restriction.

Though it may appear that `obj['foo']` can bypass visibility in the TypeScript compiler, this pattern can be broken by rearranging the build rules, and also violates [optimization compatibility](#optimization-compatibility).

### Getters and Setters (Accessors)

Getters and setters for class members may be used. The getter method must be a [pure function](https://en.wikipedia.org/wiki/Pure_function) (i.e., result is consistent and has no side effects). They are also useful as a means of restricting the visibility of internal or verbose implementation details (shown below).
```
    class Foo {
      constructor(private readonly someService: SomeService) {}
    
      get someMember(): string {
        return this.someService.someVariable;
      }
    
      set someMember(newValue: string) {
        this.someService.someVariable = newValue;
      }
    }
 ```   

If an accessor is used to hide a class property, the hidden property may be prefixed or suffixed with any whole word, like `internal` or `wrapped`. When using these private properties, access the value through the accessor whenever possible. At least one accessor for a property must be non-trivial: do not define pass-through accessors only for the purpose of hiding a property. Instead, make the property public (or consider making it `readonly` rather than just defining a getter with no setter).
```
    class Foo {
      private wrappedBar = '';
      get bar() {
        return this.wrappedBar || 'bar';
      }
    
      set bar(wrapped: string) {
        this.wrappedBar = wrapped.trim();
      }
    }
    

    class Bar {
      private barInternal = '';
      // Neither of these accessors have logic, so just make bar public.
      get bar() {
        return this.barInternal;
      }
    
      set bar(value: string) {
        this.barInternal = value;
      }
    }
   ``` 

## Primitive Types & Wrapper Classes

TypeScript code must not instantiate the wrapper classes for the primitive types `String`, `Boolean`, and `Number`. Wrapper classes have surprising behaviour, such as `new Boolean(false)` evaluating to `true`.
```
    const s = new String('hello');
    const b = new Boolean(false);
    const n = new Number(5);
    

    const s = 'hello';
    const b = false;
    const n = 5;
 ```   

## Array constructor

TypeScript code must not use the `Array()` constructor, with or without `new`. It has confusing and contradictory usage:
```
    const a = new Array(2); // [undefined, undefined]
    const b = new Array(2, 3); // [2, 3];
```    

Instead, always use bracket notation to initialize arrays, or `from` to initialize an `Array` with a certain size:
```
    const a = [2];
    const b = [2, 3];
    
    // Equivalent to Array(2):
    const c = [];
    c.length = 2;
    
    // [0, 0, 0, 0, 0]
    Array.from<number>({length: 5}).fill(0);
 ```   

## Type coercion

TypeScript code may use the `String()` and `Boolean()` (note: no `new`!) functions, string template literals, or `!!` to coerce types.
```
    const bool = Boolean(false);
    const str = String(aNumber);
    const bool2 = !!str;
    const str2 = `result: ${bool2}`;
 ```   

Using string concatenation to cast to string is discouraged, as we check that operands to the plus operator are of matching types.

Code must use `Number()` to parse numeric values, and _must_ check its return for `NaN` values explicitly, unless failing to parse is impossible from context.

Note: `Number('')`, `Number(' ')`, and `Number('\t')` would return `0` instead of `NaN`. `Number('Infinity')` and `Number('-Infinity')` would return `Infinity` and `-Infinity` respectively. These cases may require special handling.
```
    const aNumber = Number('123');
    if (isNaN(aNumber)) throw new Error(...);  // Handle NaN if the string might not contain a number
    assertFinite(aNumber, ...);                // Optional: if NaN cannot happen because it was validated before.
  ```  

Code must not use unary plus (`+`) to coerce strings to numbers. Parsing numbers can fail, has surprising corner cases, and can be a code smell (parsing at the wrong layer). A unary plus is too easy to miss in code reviews given this.
```
    const x = +y;
 ```   

Code must also not use `parseInt` or `parseFloat` to parse numbers, except for non-base-10 strings (see below). Both of those functions ignore trailing characters in the string, which can shadow error conditions (e.g. parsing `12 dwarves` as `12`).
```
    const n = parseInt(someString, 10);  // Error prone,
    const f = parseFloat(someString);    // regardless of passing a radix.
  ```  

Code that must parse using a radix _must_ check that its input is a number before calling into `parseInt`;
```
    if (!/^[a-fA-F0-9]+$/.test(someString)) throw new Error(...);
    // Needed to parse hexadecimal.
    // tslint:disable-next-line:ban
    const n = parseInt(someString, 16);  // Only allowed for radix != 10
 ```   

Use `Number()` followed by `Math.floor` or `Math.trunc` (where available) to parse integer numbers:
```
    let f = Number(someString);
    if (isNaN(f)) handleError();
    f = Math.floor(f);
 ```   

Do not use explicit boolean coercions in conditional clauses that have implicit boolean coercion. Those are the conditions in an `if`, `for` and `while` statements.
```
    const foo: MyInterface|null = ...;
    if (!!foo) {...}
    while (!!foo) {...}
    

    const foo: MyInterface|null = ...;
    if (foo) {...}
    while (foo) {...}
 ```   

Code may use explicit comparisons:
```
    // Explicitly comparing > 0 is OK:
    if (arr.length > 0) {...}
    // so is relying on boolean coercion:
    if (arr.length) {...}
```

## @ts-ignore

Do not use `@ts-ignore`. It superficially seems to be an easy way to fix a compiler error, but in practice, a specific compiler error is often caused by a larger problem that can be fixed more directly.

For example, if you are using `@ts-ignore` to suppress a type error, then it's hard to predict what types the surrounding code will end up seeing. For many type errors, the advice in [how to best use `any`](#any) is useful.


## Type and Non-nullability Assertions

Type assertions (`x as SomeType`) and non-nullability assertions (`y!`) are unsafe. Both only silence the TypeScript compiler, but do not insert any runtime checks to match these assertions, so they can cause your program to crash at runtime.

Because of this, you _should not_ use type and non-nullability assertions without an obvious or explicit reason for doing so.

Instead of the following:
```
    (x as Foo).foo();
    
    y!.bar();
 ```   

When you want to assert a type or non-nullability the best answer is to explicitly write a runtime check that performs that check.
```
    // assuming Foo is a class.
    if (x instanceof Foo) {
      x.foo();
    }
    
    if (y) {
      y.bar();
    }
  ```  

Sometimes due to some local property of your code you can be sure that the assertion form is safe. In those situations, you _should_ add clarification to explain why you are ok with the unsafe behavior:
```
    // x is a Foo, because ...
    (x as Foo).foo();
    
    // y cannot be null, because ...
    y!.bar();
    ```

If the reasoning behind a type or non-nullability assertion is obvious, the comments may not be necessary. For example, generated proto code is always nullable, but perhaps it is well-known in the context of the code that certain fields are always provided by the backend. Use your judgement.

## Type Assertions Syntax

Type assertions must use the `as` syntax (as opposed to the angle brackets syntax). This enforces parentheses around the assertion when accessing a member.
```
    const x = (<Foo>z).length;
    const y = <Foo>z.length;
    

    const x = (z as Foo).length;
    
```
## Type Assertions and Object Literals

Use type annotations (`: Foo`) instead of type assertions (`as Foo`) to specify the type of an object literal. This allows detecting refactoring bugs when the fields of an interface change over time.
```
    interface Foo {
      bar: number;
      baz?: string;  // was "bam", but later renamed to "baz".
    }
    
    const foo = {
      bar: 123,
      bam: 'abc',  // no error!
    } as Foo;
    
    function func() {
      return {
        bar: 123,
        bam: 'abc',  // no error!
      } as Foo;
    }
    

    interface Foo {
      bar: number;
      baz?: string;
    }
    
    const foo: Foo = {
      bar: 123,
      bam: 'abc',  // complains about "bam" not being defined on Foo.
    };
    
    function func(): Foo {
      return {
        bar: 123,
        bam: 'abc',   // complains about "bam" not being defined on Foo.
      };
    }
  ```  

## Member property declarations

Interface and class declarations must use the `;` character to separate individual member declarations:
```
    interface Foo {
      memberA: string;
      memberB: number;
    }
  ```  

Interfaces specifically must not use the `,` character to separate fields, for symmetry with class declarations:
```
    interface Foo {
      memberA: string,
      memberB: number,
    }
   ``` 

Inline object type declarations must use the comma as a separator:
```
    type SomeTypeAlias = {
      memberA: string,
      memberB: number,
    };
    
    let someProperty: {memberC: string, memberD: number};
   ``` 

## Optimization compatibility for property access

Code must not mix quoted property access with dotted property access:
```
    // Bad: code must use either non-quoted or quoted access for any property
    // consistently across the entire application:
    console.log(x['someField']);
    console.log(x.someField);
    ```

Code must not rely on disabling renaming, but must rather declare all properties that are external to the application to prevent renaming:

Prefer for code to account for a possible property-renaming optimization, and declare all properties that are external to the application to prevent renaming:
```
    // Good: declaring an interface
    declare interface ServerInfoJson {
      appVersion: string;
      user: UserJson;
    }
    const data = JSON.parse(serverResponse) as ServerInfoJson;
    console.log(data.appVersion); // Type safe & renaming safe!
    ```
## Enums

Always use `enum` and not `const enum`. TypeScript enums already cannot be mutated; `const enum` is a separate language feature related to optimization that makes the enum invisible to JavaScript users of the module.


Source Organization
-------------------

## Modules

### Import Paths

TypeScript code must use paths to import other TypeScript code. Paths may be relative, i.e. starting with `.` or `..`, or rooted at the base directory, e.g. `root/path/to/file`.

Code _should_ use relative imports (`./foo`) rather than absolute imports `path/to/foo` when referring to files within the same (logical) project.

Consider limiting the number of parent steps (`../../../`) as those can make module and path structures hard to understand.
```
    import {Symbol1} from 'google3/path/from/root';
    import {Symbol2} from '../parent/file';
    import {Symbol3} from './sibling';
  ```  

### Namespaces vs Modules

TypeScript supports two methods to organize code: _namespaces_ and _modules_, but namespaces are disallowed. google3 code must use TypeScript _modules_ (which are [ECMAScript 6 modules](http://exploringjs.com/es6/ch_modules.html)). That is, your code _must_ refer to code in other files using imports and exports of the form `import {foo} from 'bar';`

Your code must not use the `namespace Foo { ... }` construct. `namespace`s may only be used when required to interface with external, third party code. To semantically namespace your code, use separate files.

Code must not use `require` (as in `import x = require('...');`) for imports. Use ES6 module syntax.
```
    // Bad: do not use namespaces:
    namespace Rocket {
      function launch() { ... }
    }
    
    // Bad: do not use <reference>
    /// <reference path="..."/>
    
    // Bad: do not use require()
    import x = require('mydep');
 ```   

> NB: TypeScript `namespace`s used to be called internal modules and used to use the `module` keyword in the form `module Foo { ... }`. Don't use that either. Always use ES6 imports.

## Exports

Use named exports in all code:
```
    // Use named exports:
    export class Foo { ... }
  ```  

Do not use default exports. This ensures that all imports follow a uniform pattern.
```
    // Do not use default exports:
    export default class Foo { ... } // BAD!
  ```  

Why?

Default exports provide no canonical name, which makes central maintenance difficult with relatively little benefit to code owners, including potentially decreased readability:
```
    import Foo from './bar';  // Legal.
    import Bar from './bar';  // Also legal.
   ``` 

Named exports have the benefit of erroring when import statements try to import something that hasn't been declared. In `foo.ts`:
```
    const foo = 'blah';
    export default foo;
 ```   

And in `bar.ts`:
```
    import {fizz} from './foo';
  ```  

Results in `error TS2614: Module '"./foo"' has no exported member 'fizz'.` While `bar.ts`:
```
    import fizz from './foo';
  ```  

Results in `fizz === foo`, which is probably unexpected and difficult to debug.

Additionally, default exports encourage people to put everything into one big object to namespace it all together:
```
    export default class Foo {
      static SOME_CONSTANT = ...
      static someHelpfulFunction() { ... }
      ...
    }
  ```  

With the above pattern, we have file scope, which can be used as a namespace. We also have a perhaps needless second scope (the class `Foo`) that can be ambiguously used as both a type and a value in other files.

Instead, prefer use of file scope for namespacing, as well as named exports:

```
    export const SOME_CONSTANT = ...
    export function someHelpfulFunction()
    export class Foo {
      // only class stuff here
    }
 ```   

### Export visibility

TypeScript does not support restricting the visibility for exported symbols. Only export symbols that are used outside of the module. Generally minimize the exported API surface of modules.

### Mutable Exports

Regardless of technical support, mutable exports can create hard to understand and debug code, in particular with re-exports across multiple modules. One way to paraphrase this style point is that `export let` is not allowed.
```
    export let foo = 3;
    // In pure ES6, foo is mutable and importers will observe the value change after a second.
    // In TS, if foo is re-exported by a second file, importers will not see the value change.
    window.setTimeout(() => {
      foo = 4;
    }, 1000 /* ms */);
```

If one needs to support externally accessible and mutable bindings, they should instead use explicit getter functions.
```
    let foo = 3;
    window.setTimeout(() => {
      foo = 4;
    }, 1000 /* ms */);
    // Use an explicit getter to access the mutable export.
    export function getFoo() { return foo; };
   ``` 

For the common pattern of conditionally exporting either of two values, first do the conditional check, then the export. Make sure that all exports are final after the module's body has executed.
```
    function pickApi() {
      if (useOtherApi()) return OtherApi;
      return RegularApi;
    }
    export const SomeApi = pickApi();
   ``` 

#### Container Classes

Do not create container classes with static methods or properties for the sake of namespacing.
```
    export class Container {
      static FOO = 1;
      static bar() { return 1; }
    }
 ```   

Instead, export individual constants and functions:
```
    export const FOO = 1;
    export function bar() { return 1; }
   ``` 

## Imports

There are four variants of import statements in ES6 and TypeScript:

Import type

Example

Use for

module

`import * as foo from '...';`

TypeScript imports

destructuring

`import {SomeThing} from '...';`

TypeScript imports

default

`import SomeThing from '...';`

Only for other external code that requires them

side-effect

`import '...';`

Only to import libraries for their side-effects on load (such as custom elements)
```
    // Good: choose between two options as appropriate (see below).
    import * as ng from '@angular/core';
    import {Foo} from './foo';
    
    // Only when needed: default imports.
    import Button from 'Button';
    
    // Sometimes needed to import libraries for their side effects:
    import 'jasmine';
    import '@polymer/paper-button';
  ```  

### Module versus destructuring imports

Both module and destructuring imports have advantages depending on the situation.

Despite the `*`, a module import is not comparable to a wildcard import as seen in other languages. Instead, module imports give a name to the entire module and each symbol reference mentions the module, which can make code more readable and gives autocompletion on all symbols in a module. They also require less import churn (all symbols are available), fewer name collisions, and allow terser names in the module that's imported. Module imports are particularly useful when using many different symbols from large APIs.

Destructuring imports give local names for each imported symbol. They allow terser code when using the imported symbol, which is particularly useful for very commonly used symbols, such as Jasmine's `describe` and `it`.
```
    // Bad: overlong import statement of needlessly namespaced names.
    import {TableViewItem, TableViewHeader, TableViewRow, TableViewModel,
      TableViewRenderer} from './tableview';
    let item: TableViewItem = ...;
    

    // Better: use the module for namespacing.
    import * as tableview from './tableview';
    let item: tableview.Item = ...;
    

    import * as testing from './testing';
    
    // All tests will use the same three functions repeatedly.
    // When importing only a few symbols that are used very frequently, also
    // consider importing the symbols directly (see below).
    testing.describe('foo', () => {
      testing.it('bar', () => {
        testing.expect(...);
        testing.expect(...);
      });
    });
    

    // Better: give local names for these common functions.
    import {describe, it, expect} from './testing';
    
    describe('foo', () => {
      it('bar', () => {
        expect(...);
        expect(...);
      });
    });
    ...
   ``` 

### Renaming imports

Code _should_ fix name collisions by using a module import or renaming the exports themselves. Code _may_ rename imports (`import {SomeThing as SomeOtherThing}`) if needed.

Three examples where renaming can be helpful:

1.  If it's necessary to avoid collisions with other imported symbols.
2.  If the imported symbol name is generated.
3.  If importing symbols whose names are unclear by themselves, renaming can improve code clarity. For example, when using RxJS the `from` function might be more readable when renamed to `observableFrom`.

### Import & export type

Do not use `import type ... from` or `export type ... from`.

Note: this does not apply to exporting type definitions, i.e. `export type Foo = ...;`.
```
    import type {Foo} from './foo';
    export type {Bar} from './bar';
  ```  

Instead, just use regular imports:
```
    import {Foo} from './foo';
    export {Bar} from './bar';
   ``` 

TypeScript tooling automatically distinguishes symbols used as types vs symbols used as values and only generates runtime loads for the latter.

Why?

TypeScript tooling automatically handles the distinction and does not insert runtime loads for type references. This gives a better developer UX: toggling back and forth between `import type` and `import` is bothersome. At the same time, `import type` gives no guarantees: your code might still have a hard dependency on some import through a different transitive path.

If you need to force a runtime load for side effects, use `import '...';`. See

`export type` might seem useful to avoid ever exporting a value symbol for an API. However it does not give guarantees either: downstream code might still import an API through a different path. A better way to split & guarantee type vs value usages of an API is to actually split the symbols into e.g. `UserService` and `AjaxUserService`. This is less error prone and also better communicates intent.

## Organize By Feature

Organize packages by feature, not by type. For example, an online shop _should_ have packages named `products`, `checkout`, `backend`, not `views`, `models`, `controllers`.

Type System
-----------

## Type Inference

Code may rely on type inference as implemented by the TypeScript compiler for all type expressions (variables, fields, return types, etc). The google3 compiler flags reject code that does not have a type annotation and cannot be inferred, so all code is guaranteed to be typed (but might use the `any` type explicitly).
```
    const x = 15;  // Type inferred.
 ```   

Leave out type annotations for trivially inferred types: variables or parameters initialized to a `string`, `number`, `boolean`, `RegExp` literal or `new` expression.
```
    const x: boolean = true;  // Bad: 'boolean' here does not aid readability
    

    // Bad: 'Set' is trivially inferred from the initialization
    const x: Set<string> = new Set();
    

    const x = new Set<string>();
  ```  

For more complex expressions, type annotations can help with readability of the program. Whether an annotation is required is decided by the code reviewer.

### Return types

Whether to include return type annotations for functions and methods is up to the code author. Reviewers _may_ ask for annotations to clarify complex return types that are hard to understand. Projects _may_ have a local policy to always require return types, but this is not a general TypeScript style requirement.

There are two benefits to explicitly typing out the implicit return values of functions and methods:

*   More precise documentation to benefit readers of the code.
*   Surface potential type errors faster in the future if there are code changes that change the return type of the function.

## Null vs Undefined

TypeScript supports `null` and `undefined` types. Nullable types can be constructed as a union type (`string|null`); similarly with `undefined`. There is no special syntax for unions of `null` and `undefined`.

TypeScript code can use either `undefined` or `null` to denote absence of a value, there is no general guidance to prefer one over the other. Many JavaScript APIs use `undefined` (e.g. `Map.get`), while many DOM and Google APIs use `null` (e.g. `Element.getAttribute`), so the appropriate absent value depends on the context.

### Nullable/undefined type aliases

Type aliases _must not_ include `|null` or `|undefined` in a union type. Nullable aliases typically indicate that null values are being passed around through too many layers of an application, and this clouds the source of the original issue that resulted in `null`. They also make it unclear when specific values on a class or interface might be absent.

Instead, code _must_ only add `|null` or `|undefined` when the alias is actually used. Code _should_ deal with null values close to where they arise, using the above techniques.
```
    // Bad
    type CoffeeResponse = Latte|Americano|undefined;
    
    class CoffeeService {
      getLatte(): CoffeeResponse { ... };
    }
    

    // Better
    type CoffeeResponse = Latte|Americano;
    
    class CoffeeService {
      getLatte(): CoffeeResponse|undefined { ... };
    }
    

    // Best
    type CoffeeResponse = Latte|Americano;
    
    class CoffeeService {
      getLatte(): CoffeeResponse {
        return assert(fetchResponse(), 'Coffee maker is broken, file a ticket');
      };
    }
   ``` 

### Optionals vs `|undefined` type

In addition, TypeScript supports a special construct for optional parameters and fields, using `?`:
```
    interface CoffeeOrder {
      sugarCubes: number;
      milk?: Whole|LowFat|HalfHalf;
    }
    
    function pourCoffee(volume?: Milliliter) { ... }
 ```   

Optional parameters implicitly include `|undefined` in their type. However, they are different in that they can be left out when constructing a value or calling a method. For example, `{sugarCubes: 1}` is a valid `CoffeeOrder` because `milk` is optional.

Use optional fields (on interfaces or classes) and parameters rather than a `|undefined` type.

For classes preferably avoid this pattern altogether and initialize as many fields as possible.
```
    class MyClass {
      field = '';
    }
 ```   

## Structural Types vs Nominal Types

TypeScript's type system is structural, not nominal. That is, a value matches a type if it has at least all the properties the type requires and the properties' types match, recursively.

Use structural typing where appropriate in your code. Outside of test code, use interfaces to define structural types, not classes. In test code it can be useful to have mock implementations structurally match the code under test without introducing an extra interface.

When providing a structural-based implementation, explicitly include the type at the declaration of the symbol (this allows more precise type checking and error reporting).
```
    const foo: Foo = {
      a: 123,
      b: 'abc',
    }
    

    const badFoo = {
      a: 123,
      b: 'abc',
    }
   ``` 

Why?

The badFoo object above relies on type inference. Additional fields could be added to badFoo and the type is inferred based on the object itself.

When passing a badFoo to a function that takes a Foo, the error will be at the function call site, rather than at the object declaration site. This is also useful when changing the surface of an interface across broad codebases.
```
    interface Animal {
      sound: string;
      name: string;
    }
    
    function makeSound(animal: Animal) {}
    
    /**
     * 'cat' has an inferred type of '{sound: string}'
     */
    const cat = {
      sound: 'meow',
    };
    
    /**
     * 'cat' does not meet the type contract required for the function, so the
     * TypeScript compiler errors here, which may be very far from where 'cat' is
     * defined.
     */
    makeSound(cat);
    
    /**
     * Horse has a structural type and the type error shows here rather than the
     * function call.  'horse' does not meet the type contract of 'Animal'.
     */
    const horse: Animal = {
      sound: 'niegh',
    };
    
    const dog: Animal = {
      sound: 'bark',
      name: 'MrPickles',
    };
    
    makeSound(dog);
    makeSound(horse);
 ```   

## Interfaces vs Type Aliases

TypeScript supports [type aliases](https://www.typescriptlang.org/docs/handbook/advanced-types.html#type-aliases) for naming a type expression. This can be used to name primitives, unions, tuples, and any other types.

However, when declaring types for objects, use interfaces instead of a type alias for the object literal expression.
```
    interface User {
      firstName: string;
      lastName: string;
    }
    

    type User = {
      firstName: string,
      lastName: string,
    }
 ```   

Why?

These forms are nearly equivalent, so under the principle of just choosing one out of two forms to prevent variation, we should choose one. Additionally, there also [interesting technical reasons to prefer interface](https://ncjamieson.com/prefer-interfaces/). That page quotes the TypeScript team lead: Honestly, my take is that it should really just be interfaces for anything that they can model. There is no benefit to type aliases when there are so many issues around display/perf.

## `Array<T>` Type

For simple types (containing just alphanumeric characters and dot), use the syntax sugar for arrays, `T[]`, rather than the longer form `Array<T>`.

For anything more complex, use the longer form `Array<T>`.

This also applies for `readonly T[]` vs `ReadonlyArray<T>`.
```
    const a: string[];
    const b: readonly string[];
    const c: ns.MyObj[];
    const d: Array<string|number>;
    const e: ReadonlyArray<string|number>;
    

    const f: Array<string>;            // the syntax sugar is shorter
    const g: ReadonlyArray<string>;
    const h: {n: number, s: string}[]; // the braces/parens make it harder to read
    const i: (string|number)[];
    const j: readonly (string|number)[];
  ```  

## Indexable (`{[key: string]: number}`) Type

In JavaScript, it's common to use an object as an associative array (aka map, hash, or dict):

    const fileSizes: {[fileName: string]: number} = {};
    fileSizes['readme.txt'] = 541;
    

In TypeScript, provide a meaningful label for the key. (The label only exists for documentation; it's unused otherwise.)
```
    const users: {[key: string]: number} = ...;
    

    const users: {[userName: string]: number} = ...;
  ```  

> Rather than using one of these, consider using the ES6 `Map` and `Set` types instead. JavaScript objects have [surprising undesirable behaviors](http://2ality.com/2012/01/objects-as-maps.html) and the ES6 types more explicitly convey your intent. Also, `Map`s can be keyed by—and `Set`s can contain—types other than `string`.

TypeScript's builtin `Record<Keys, ValueType>` type allows constructing types with a defined set of keys. This is distinct from associative arrays in that the keys are statically known. See advice on that [below](#mapped-conditional-types).

## Mapped & Conditional Types

TypeScript's [mapped types](https://www.typescriptlang.org/docs/handbook/advanced-types.html#mapped-types) and [conditional types](https://www.typescriptlang.org/docs/handbook/advanced-types.html#conditional-types) allow specifying new types based on other types. TypeScript's standard library includes several type operators based on these (`Record`, `Partial`, `Readonly` etc).

These type system features allow succinctly specifying types and constructing powerful yet type safe abstractions. They come with a number of drawbacks though:

*   Compared to explicitly specifying properties and type relations (e.g. using interfaces and extension, see below for an example), type operators require the reader to mentally evaluate the type expression. This can make programs substantially harder to read, in particular combined with type inference and expressions crossing file boundaries.
*   Mapped & conditional types' evaluation model, in particular when combined with type inference, is underspecified, not always well understood, and often subject to change in TypeScript compiler versions. Code can accidentally compile or seem to give the right results. This increases future support cost of code using type operators.
*   Mapped & conditional types are most powerful when deriving types from complex and/or inferred types. On the flip side, this is also when they are most prone to create hard to understand and maintain programs.
*   Some language tooling does not work well with these type system features. E.g. your IDE's find references (and thus rename property refactoring) will not find properties in a `Pick<T, Keys>` type, and Code Search won't hyperlink them.

The style recommendation is:

*   Always use the simplest type construct that can possibly express your code.
*   A little bit of repetition or verbosity is often much cheaper than the long term cost of complex type expressions.
*   Mapped & conditional types may be used, subject to these considerations.

For example, TypeScript's builtin `Pick<T, Keys>` type allows creating a new type by subsetting another type `T`, but simple interface extension can often be easier to understand.
```
    interface User {
      shoeSize: number;
      favoriteIcecream: string;
      favoriteChocolate: string;
    }
    
    // FoodPreferences has favoriteIcecream and favoriteChocolate, but not shoeSize.
    type FoodPreferences = Pick<User, 'favoriteIcecream'|'favoriteChocolate'>;
  ```  

This is equivalent to spelling out the properties on `FoodPreferences`:
```
    interface FoodPreferences {
      favoriteIcecream: string;
      favoriteChocolate: string;
    }
 ```   

To reduce duplication, `User` could extend `FoodPreferences`, or (possibly better) nest a field for food preferences:
```
    interface FoodPreferences { /* as above */ }
    interface User extends FoodPreferences {
      shoeSize: number;
      // also includes the preferences.
    }
  ```  

Using interfaces here makes the grouping of properties explicit, improves IDE support, allows better optimization, and arguably makes the code easier to understand.

## `any` Type

TypeScript's `any` type is a super and subtype of all other types, and allows dereferencing all properties. As such, `any` is dangerous - it can mask severe programming errors, and its use undermines the value of having static types in the first place.

**Consider _not_ to use `any`.** In circumstances where you want to use `any`, consider one of:

*   [Provide a more specific type](#any-specific)
*   [Use `unknown`](#any-unknown)
*   [Suppress the lint warning and document why](#any-suppress)

### Providing a more specific type

Use interfaces , an inline object type, or a type alias:
```
    // Use declared interfaces to represent server-side JSON.
    declare interface MyUserJson {
      name: string;
      email: string;
    }
    
    // Use type aliases for types that are repetitive to write.
    type MyType = number|string;
    
    // Or use inline object types for complex returns.
    function getTwoThings(): {something: number, other: string} {
      // ...
      return {something, other};
    }
    
    // Use a generic type, where otherwise a library would say `any` to represent
    // they don't care what type the user is operating on (but note "Return type
    // only generics" below).
    function nicestElement<T>(items: T[]): T {
      // Find the nicest element in items.
      // Code can also put constraints on T, e.g. <T extends HTMLElement>.
    }
 ```   

### Using `unknown` over `any`

The `any` type allows assignment into any other type and dereferencing any property off it. Often this behaviour is not necessary or desirable, and code just needs to express that a type is unknown. Use the built-in type `unknown` in that situation — it expresses the concept and is much safer as it does not allow dereferencing arbitrary properties.
```
    // Can assign any value (including null or undefined) into this but cannot
    // use it without narrowing the type or casting.
    const val: unknown = value;
    

    const danger: any = value /* result of an arbitrary expression */;
    danger.whoops();  // This access is completely unchecked!
```    

To safely use `unknown` values, narrow the type using a [type guard](https://www.typescriptlang.org/docs/handbook/advanced-types.html#type-guards-and-differentiating-types)

### Suppressing `any` lint warnings

Sometimes using `any` is legitimate, for example in tests to construct a mock object. In such cases, add a comment that suppresses the lint warning, and document why it is legitimate.
```
    // This test only needs a partial implementation of BookService, and if
    // we overlooked something the test will fail in an obvious way.
    // This is an intentionally unsafe partial mock
    // tslint:disable-next-line:no-any
    const mockBookService = ({get() { return mockBook; }} as any) as BookService;
    // Shopping cart is not used in this test
    // tslint:disable-next-line:no-any
    const component = new MyComponent(mockBookService, /* unused ShoppingCart */ null as any);
 ```   

## Tuple Types

If you are tempted to create a Pair type, instead use a tuple type:
```
    interface Pair {
      first: string;
      second: string;
    }
    function splitInHalf(input: string): Pair {
      ...
      return {first: x, second: y};
    }
    

    function splitInHalf(input: string): [string, string] {
      ...
      return [x, y];
    }
    
    // Use it like:
    const [leftHalf, rightHalf] = splitInHalf('my string');
  ```  

However, often it's clearer to provide meaningful names for the properties.

If declaring an `interface` is too heavyweight, you can use an inline object literal type:
```
    function splitHostPort(address: string): {host: string, port: number} {
      ...
    }
    
    // Use it like:
    const address = splitHostPort(userAddress);
    use(address.port);
    
    // You can also get tuple-like behavior using destructuring:
    const {host, port} = splitHostPort(userAddress);
  ```  

## Wrapper types

There are a few types related to JavaScript primitives that should never be used:

*   `String`, `Boolean`, and `Number` have slightly different meaning from the corresponding primitive types `string`, `boolean`, and `number`. Always use the lowercase version.
*   `Object` has similarities to both `{}` and `object`, but is slightly looser. Use `{}` for a type that include everything except `null` and `undefined`, or lowercase `object` to further exclude the other primitive types (the three mentioned above, plus `symbol` and `bigint`).

Further, never invoke the wrapper types as constructors (with `new`).

## Return type only generics

Avoid creating APIs that have return type only generics. When working with existing APIs that have return type only generics always explicitly specify the generics.
